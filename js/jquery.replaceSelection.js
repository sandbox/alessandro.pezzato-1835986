/**
 * Original code from Alex Brem [jquery-fieldselection](https://github.com/localhost/jquery-fieldselection)
 */
(function($) {
  $.fn.replaceSelection = function() {
    var e = (this.jquery) ? this[0] : this;
    var text = arguments[0] || '';
    return (
    /* mozilla / dom 3.0 */
    ('selectionStart' in e && function() {
      e.value = e.value.substr(0, e.selectionStart) + text
          + e.value.substr(e.selectionEnd, e.value.length);
      return this;
    })
        ||
        /* explorer */
        (document.selection && function() {
          e.focus();
          document.selection.createRange().text = text;
          return this;
        }) ||
    /* browser not supported */
    function() {
      e.value += text;
      return jQuery(e);
    })();
  };
})(jQuery);
