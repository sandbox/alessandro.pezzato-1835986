/**
 * Script related to StackEdit display
 */
(function($) {
  $(document).ready(function() {
    prettify();
    $('.node-content code').addClass('inline-code');
    $('.prettyprint code').removeClass('inline-code');
    $('.node-content code').codeMetadata();
  });
  /**
   * enable google-code-prettify
   */
  function prettify() {
    /* TODO: check if this selector is enough */
    $('.node-content pre').addClass('prettyprint');
    if (typeof prettyPrint === 'function') {
      prettyPrint();
    }
  }
})(jQuery)
