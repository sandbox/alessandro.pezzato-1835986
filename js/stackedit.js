/**
 * Global object for StackEdit module
 */
var StackEdit = {
  t : function(text) {
    var translated = Drupal.settings.stackedit.i18n[text];
    return translated !== undefined ? translated : text;
  }
}
