/**
 * @name jquery.codeMetadata
 * @descripton jQuery plugin, add metadata information to code blocks
 * 
 * @author Alessandro Pezzato
 * @author-email alessandro.pezzato@gmail.com
 * @author-website http://alessandro.pezzato.net
 * 
 * @licence GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 */
(function($) {
  $.fn.codeMetadata = function(options) {
    var settings = $.extend({
      'metadata' : 'html-comment'
    }, options);
    return this.each(function() {
      $(this).data('codeMetadata', new CodeMetadata(this));
    });
  };
  /**
   * constructor of CodeMetadata object
   */
  function CodeMetadata(element) {
    this.element = element;
    var $el = $(element);
    function searchMetadata() {
      var $parent = $el.parent().parent();
      $parent.contents().each(function(index, node) {
        if (node.nodeType == 8) {
          if (node.nextSibling.nextSibling.nodeName == 'PRE') {
            var $pre = $(node.nextSibling.nextSibling);
            var $code = $('code', $pre);
            var data = node.data;
            /* check if comment starts with 'code : ' */
            var colonPos = data.indexOf(':');
            if (data.substr(0, colonPos).indexOf('code') < 0) {
              return;
            }
            // console.log('"' + node.data.substr(colonPos + 1) + '"');
            var metadataStr = node.data.substr(colonPos + 1);
            metadataStr = $.trim(metadataStr);
            if (metadataStr[0] != '{') {
              console.log(metadataStr[0]);
              return;
            }
            if (metadataStr[metadataStr.length - 1] != '}') {
              return;
            }
            metadataStr = metadataStr.substr(1, metadataStr.length - 2);
            var arr = metadataStr.split(',');
            for ( var i = 0; i != arr.length; ++i) {
              var pair = arr[i].split(':');
              var handle = $.fn.codeMetadata.handles[$.trim(pair[0])];
              if (handle !== undefined) {
                handle($code, $.trim(pair[1]));
              }
            }
            // switch (key) {
            // case 'language':
            // $code.addClass('prettyprint').addClass(val);
            // break;
            // case 'file':
            // $pre.before(createFileHeader(val));
            // break;
            // }
            // }
            $(node).remove();
          }
        }
      });
    }
    searchMetadata();
  }
  $.fn.codeMetadata.handles = {};
  /**
   * 
   */
  $.fn.codeMetadata.handles['lang'] = function($code, language) {
    $code.addClass('lang-' + language);
  };
  /**
   * 
   */
  $.fn.codeMetadata.handles['file'] = function($code, filename) {
    $code.parent().before(
        $('<div class="code-header code-filename-header">file: <strong>' + filename
            + '</strong></div>'));
  };
  /**
   * 
   */
  $.fn.codeMetadata.handles['out'] = function($code, flag) {
    if (flag) {
      $code.parent().addClass('code-out');
      $code.parent().before($('<div class="code-header code-out-header">output</div>'));
    }
  };
})(jQuery);