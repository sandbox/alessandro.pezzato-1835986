/**
* @name           jquery.autoIndent
* @descripton     jQuery plugin, enable autoindent on textarea
*
* @author         Alessandro Pezzato
* @author-email   alessandro.pezzato@gmail.com
* @author-website http://alessandro.pezzato.net
*
* @licence        GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
*/
(function($) {
  /**
   * @param options
   * 
   * If options is 'disabled', plugin is disabled.
   */
  $.fn.autoIndent = function(options) {
    if (options === 'disable') {
      return this.each(disable);
    }
    return this.each(enable);
  };
  /**
   * Initialize auto indent plugin
   */
  function enable(index, element) {
    var $self = $(element);
    if ($self[0].tagName != 'TEXTAREA') {
      /* ignore if not a textarea */
      return;
    }
    $self.bind('keypress', onKeyPress);
    $self.data('autoIndentEnabled', true);
  }
  /**
   * Disable auto indent
   */
  function disable(index, element) {
    var $self = $(element);
    if ($self.data('autoIndentEnabled') != true) {
      return;
    }
    $self.unbind('keypress', onKeyPress);
    $self.data('autoIndentEnabled', false);
  }
  /**
   * Called when key is pressed inside textarea
   * 
   * @param e
   *          the event object
   */
  function onKeyPress(e) {
    if ('change', e.which == 13) {
      e.preventDefault();
      /* newline detected (key 13) */
      var el = e.currentTarget;
      var pos = getCaretPosition(el);
      var text = el.value;
      /* find previous newline */
      var newLinePos = 0;
      for ( var i = pos - 1; i >= 0; --i) {
        if (text[i] == '\n') {
          newLinePos = i;
          break;
        }
      }
      /* count spaces from newline */
      var indStr = '';
      var indLen = 0;
      for ( var i = newLinePos + 1; i <= pos; ++i) {
        if (text[i] == ' ') {
          indStr += ' ';
          ++indLen;
        } else {
          break;
        }
      }
      /* insert indentation */
      var before = text.substring(0, pos);
      var after = text.substring(pos);
      el.value = before + '\n' + indStr + after;
      /* reposition caret */
      $(el).setCaretPosition(pos + indLen + 1);
    }
  }
  ;
  /**
   * Find current caret position
   * 
   * @param element
   *          the textarea element
   */
  function getCaretPosition(element) {
    var pos = 0;
    if ('selectionStart' in element) {
      pos = element.selectionStart;
    } else if ('selection' in document) {
      element.focus();
      var Sel = document.selection.createRange();
      var SelLength = document.selection.createRange().text.length;
      Sel.moveStart('character', -element.value.length);
      pos = Sel.text.length - SelLength;
    }
    return pos;
  }
})(jQuery);