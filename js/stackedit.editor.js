/**
 * Script related to StackEdit editor
 */
(function($) {
  var t = StackEdit.t;
  /**
   * 
   */
  $(document).ready(function() {
    var $filterLists = $('.text-format-wrapper select.filter-list');
    $filterLists.each(handleFilterList);
    $filterLists.change(handleFilterList);
  });
  /**
   * enable StackEdit editor on selected textarea
   * 
   * @param $textarea
   *          a jQuery object containing a textarea (or more)
   */
  function enableEditor($textarea) {
    $textarea.addClass('stackedit-editor-textarea');
    $textarea.autoIndent();
    var toolbar = new Toolbar($textarea);
    $textarea.data('toolbar', toolbar);
    var previewer = new Previewer($textarea);
    $textarea.data('previewer', previewer);
    $textarea.elastic();
  }
  /**
   * disable StackEdit editor on selected textarea
   * 
   * @param $textarea
   *          a jQuery object containing a textarea (or more)
   */
  function disableEditor($textarea) {
    $textarea.removeClass('stackedit-editor-textarea');
    $textarea.autoIndent('disable');
  }
  /**
   * enable StackEdit editor on textarea with StackEdit text format is enabled
   * disable StackEdit editor on textarea, when StackEdit text format is
   * disabled (called on document ready and on filter-list change
   */
  function handleFilterList() {
    var $self = $(this);
    var listName = $self.attr('name');
    var textareaName = listName.replace('[format]', '[value]');
    var $textarea = $('textarea[name="' + textareaName + '"]');
    if ($('option:selected', $self).val() == 'stackedit') {
      enableEditor($textarea);
    } else {
      disableEditor($textarea);
    }
  }
  /**
   * Toolbar constructor
   */
  function Toolbar($textarea) {
    var $element = $('<ul/>', {
      'class' : 'stackedit-editor-toolbar clearfix'
    });
    $textarea.before($element);
    var buttons = [ new Button(0, 'bold', 'Bold', 'ctrl+b', function(event) {
      /* bold clicked */
      event.preventDefault();
      var sel = $textarea.getSelection();
      var text = t('bold_text_here');
      if (sel.length > 0) {
        text = sel.text;
      }
      $textarea.replaceSelection('**' + text + '**');
      $textarea.focus();
    }), new Button(1, 'italic', 'Italic', 'ctrl+i', function(event) {
      /* italic clicked */
      event.preventDefault();
      var sel = $textarea.getSelection();
      var text = t('italic_text_here');
      if (sel.length > 0) {
        text = sel.text;
      }
      $textarea.replaceSelection('**' + text + '**');
      $textarea.focus();
    }), new Button(2, 'heading1', 'Heading 1', 'ctrl+h', function(event) {
      /* heading clicked */
      event.preventDefault();
      if ($textarea.getSelection().length > 0) {
        underline($textarea, '=');
      } else {
        prependText($textarea, '# ');
      }
      $textarea.focus();
    }), new Button(3, 'heading2', 'Heading 2', null, function(event) {
      /* heading clicked */
      event.preventDefault();
      if ($textarea.getSelection().length > 0) {
        underline($textarea, '-');
      } else {
        prependText($textarea, '## ');
      }
      $textarea.focus();
    }), new Button(4, 'heading3', 'Heading 3', null, function(event) {
      /* heading clicked */
      event.preventDefault();
      prependText($textarea, '### ');
    }), new Button(5, 'quote', 'Quote', 'ctrl+q', function(event) {
      /* quote clicked */
      event.preventDefault();
      if ($textarea.getSelection().length > 0) {
        $textarea.indentSelection({
          'space' : '> ',
          'break' : true
        });
      } else {
        prependText($textarea, '> ');
      }
      $textarea.focus();
    }), new Button(6, 'code', 'Code block', 'ctrl+k', function(event) {
      /* code clicked */
      event.preventDefault();
      var sel = $textarea.getSelection();
      if (sel.length > 0) {
        if (sel.text.indexOf('\n') < 0) {
          $textarea.replaceSelection('`' + sel.text + '`');
        } else {
          $textarea.indentSelection({
            'space' : '    ',
            'break' : true
          });
        }
      } else {
        prependText($textarea, '    ');
        $textarea.setCaretPosition(sel.start + 4);
      }
      $textarea.focus();
    }), new Button(7, 'olist', 'Ordered list', 'ctrl+o', function(event) {
      /* olist clicked */
      event.preventDefault();
      if ($textarea.getSelection().length > 0) {
        $textarea.indentSelection({
          'space' : function(lineNumber) {
            return ' ' + lineNumber + '. ';
          },
          'break' : true
        });
      } else {
        prependText($textarea, ' 1. ');
      }
      $textarea.focus();
    }), new Button(8, 'ulist', 'Unordered list', 'ctrl+u', function(event) {
      /* ulist clicked */
      event.preventDefault();
      if ($textarea.getSelection().length > 0) {
        $textarea.indentSelection({
          'space' : ' - ',
          'break' : true
        });
      } else {
        prependText($textarea, ' - ');
      }
      $textarea.focus();
    }), new Button(9, 'link', 'Link', 'ctrl+l', function(event) {
      /* link clicked */
      event.preventDefault();
      var sel = $textarea.getSelection();
      if (sel.length > 0) {
        $textarea.replaceSelection('[' + sel.text + '](url)');
      } else {
        $textarea.replaceSelection('[' + t('Text') + '](url)');
      }
      $textarea.focus();
    }), new Button(10, 'email', 'Email', 'ctrl+e', function(event) {
      /* email clicked */
      event.preventDefault();
      var sel = $textarea.getSelection();
      if (sel.length > 0) {
        $textarea.replaceSelection('<' + sel.text + '>');
      } else {
        $textarea.replaceSelection('<' + t('Email address') + '>');
      }
      $textarea.focus();
    }), new Button(11, 'image', 'Image', 'ctrl+g', function(event) {
      /* image clicked */
      event.preventDefault();
      var sel = $textarea.getSelection();
      if (sel.length > 0) {
        /* TODO: image, selection */
      } else {
        /* TODO: image, no selection */
      }
      $textarea.focus();
    }), new Button(12, 'hr', 'Horizontal Rule', 'ctrl+r', function(event) {
      /* hr clicked */
      event.preventDefault();
      prependText($textarea, '\n----------------------------------------\n\n')
    }) ];
    for ( var i = 0; i < buttons.length; ++i) {
      var btn = buttons[i];
      $element.append(btn.element);
      if (btn.hotkey !== null && jQuery.hotkeys !== undefined) {
        $textarea.bind('keydown', btn.hotkey, btn.fn);
      }
    }
    this.element = $element[0];
  }
  /**
   * Button constructor
   */
  function Button(index, id, title, hotkey, fn) {
    this.id = id;
    this.title = title;
    this.hotkey = hotkey;
    this.fn = fn;
    var $el = $('<li/>', {
      'class' : 'stackedit-editor-button',
      'title' : t(title) + ' [' + hotkey + ']'
    });
    $el.addClass('stackedit-editor-button-' + id);
    $el.bind('click', fn);
    $el.css('background-position', '-' + (32 * index) + 'px 0');
    this.element = $el[0];
  }
  /**
   * add a line under current line
   * 
   * @param textarea
   *          the textarea
   * @param c
   *          the character that fill the line
   */
  function underline($textarea, c) {
    var selection = $textarea.getSelection();
    var text = $textarea.val();
    var underline = '\n';
    var end = selection.end;
    while (text[end] !== '\n' && end < text.length) {
      ++end;
      underline += c;
    }
    var begin = selection.start - 1;
    while (text[begin] !== '\n' && begin >= 0) {
      --begin;
      underline += c;
    }
    $textarea.val(text.substr(0, end) + underline + text.substr(end));
  }
  /**
   * add text at the beginning of current line
   */
  function prependText($textarea, textToPrepend) {
    var selection = $textarea.getSelection();
    var text = $textarea.val();
    var begin = selection.start - 1;
    while (text[begin] !== '\n' && begin >= 0) {
      --begin;
    }
    $textarea.val(text.substr(0, begin) + '\n' + textToPrepend
        + text.substr(begin + 1));
  }
  /**
   * Previewer constructor
   */
  function Previewer($textarea) {
    var $element = $('<div/>', {
      'class' : 'stackedit-editor-previewer'
    }).hide();
    var visible = false;
    $textarea.before($element);
    this.element = $element[0];
    function show() {
      visible = true;
      var previewURL = Drupal.settings.stackedit.previewURL;
      $.ajax({
        type : 'POST',
        url : previewURL,
        data : {
          'md' : $textarea.val()
        },
        success : function(data, textStatus, jqXHR) {
          $element.html(data);
          $element.slideDown();
        },
        dataType : 'html'
      });
    }
    function hide() {
      $element.slideUp();
      visible = false;
    }
    function toggle(event) {
      event.preventDefault();
      if (visible) {
        hide();
      } else {
        show();
      }
    }
    if (jQuery.hotkeys !== undefined) {
      $textarea.bind('keydown', 'ctrl+p', toggle);
    }
    $element.click(hide);
  }
})(jQuery)
