/**
 * @name jquery.autoIndent
 * @descripton jQuery plugin, enable autoindent on textarea
 * 
 * @author Alessandro Pezzato
 * @author-email alessandro.pezzato@gmail.com
 * @author-website http://alessandro.pezzato.net
 * 
 * @licence GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 */
(function($) {
  /**
   * @param settings
   *          <ul>
   *          <li>space: a string or a function returning a string used to
   *          indent text. Default is 4 spaces. If a function is specified, it
   *          can be in this form: function(lineNumber) {} </li>
   *          </li>
   *          <li>break: if true, break selected text, inserting a newline
   *          before and after selection.</li>
   *          </ul>
   */
  $.fn.indentSelection = function(options) {
    var settings = $.extend({
      'space' : '    ',
      'break' : false,
    }, options);
    return this.each(function() {
      var $textarea = $(this);
      var selection = $textarea.getSelection().text;
      if (selection.length == 0) {
        return;
      }
      var spaceFn = settings.space;
      if (!$.isFunction(spaceFn)) {
        spaceFn = function() {
          return settings.space;
        };
      }
      var result = '';
      var lines = selection.split('\n');
      for ( var i = 0; i < lines.length; i++) {
        if (i > 0 || settings['break']) {
          result += '\n';
        }
        result += spaceFn(i + 1) + lines[i];
      }
      if (settings['break']) {
        result += '\n';
      }
      $textarea.replaceSelection(result);
    });
  }
})(jQuery);