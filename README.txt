StackEdit README
================

SUMMARY
-------

The "StackEdit" module provides a text editor inspired by the one used in
StackOverflow site.

Provides Markdown filter integration for Drupal input formats.

Sintax-hightlighting is applied to code blocks. 

REQUIREMENTS
------------

 - google-code-prettify for syntax hightlighting  <http://code.google.com/p/google-code-prettify>

INSTALLATION
------------

Install `stackedit` module.  
Download `google-code-prettify` from <http://code.google.com/p/google-code-prettify/downloads/list> 
and extract the content of `distrib` directory into `libraries` directory. 
Or download the "small" version and extract as described below:

    wget http://google-code-prettify.googlecode.com/files/prettify-small-1-Jun-2011.tar.bz2
    tar jxvf prettify-small-1-Jun-2011.tar.bz2 -C libraries
    rm -r prettify-small-1-Jun-2011.tar.bz2

### For developers only:

If you got this module from git, remember to clone also submodules.

    git submodule status      # show repository's submodules
    git submodule init        # initialize local configuration
    git submodule update      # fetch all the data from submodules


CONFIGURATION
-------------

    TODO

CUSTOMIZATION
-------------

    TODO

TROUBLESHOOTING
---------------

    TODO

FAQ
---

    TODO

CONTACT
-------

Current maintainer:

 - [Alessandro Pezzato](http://drupal.org/user/1413564)

CREDITS
-------

 - [pagedown project](http://code.google.com/p/pagedown) inspired some 
   features of StackEdit editor
 - John Fraser, author of *Showdown*
 - John Gruber, author of *Markdown*
 - John Resig, author of *jquery* *jquery.hotkeys*
 - Alex Brem, author of
   [jquery-fieldselection](https://github.com/localhost/jquery-fieldselection)
 - Jan Jarfalk, author of
   [Elastic](http://unwrongest.com/projects/elastic/) Jquery plugin
