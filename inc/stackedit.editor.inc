<?php
/**
 * @file
 * hooks and functions relative to StackEdit editor.
 */

/**
 * Initialize editor.
 */
function _stackedit_editor_init() {
  drupal_add_js(array(
    'stackedit' => array(
      'previewURL' => $GLOBALS['base_url'] . '/stackedit/preview',
    ),
  ), 'setting');
}

/**
 * Implements hook_field_attach_form().
 */
function stackedit_field_attach_form($entity_type, $entity, &$form, &$form_state, $langcode) {
  /* add editor js only when form is used
   * TODO: maybe this can be restricted to "only when a field with text format is enabled"
  * and "user can use stackedit filter format"
  */
  _stackedit_editor_add_resources();
}

/**
 * Adds javascript and css resources.
 */
function _stackedit_editor_add_resources() {
  $path = drupal_get_path('module', 'stackedit');
  drupal_add_js($path . '/libraries/jquery.hotkeys/jquery.hotkeys.js');
  drupal_add_js($path . '/js/jquery.replaceSelection.js');
  drupal_add_js($path . '/js/jquery.getSelection.js');
  drupal_add_js($path . '/js/jquery.setCaretPosition.js');
  drupal_add_js($path . '/js/jquery.autoIndent.js');
  drupal_add_js($path . '/js/jquery.indentSelection.js');
  drupal_add_js($path . '/js/stackedit.editor.js');
  drupal_add_css($path . '/css/stackedit.editor.css');
}

/**
 * Implements of hook_menu().
 */
function stackedit_menu() {
  $items = array();
  $items['stackedit/preview'] = array(
    'delivery callback' => '_stackedit_preview',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Generate a preview, used by an ajax call.
 */
function _stackedit_preview() {
  print check_markup($_POST['md'], 'stackedit');
}
