<?php
/**
 * @file
 * hooks and functions relative to StackEdit text format.
 */

/**
 * Create the "StackEdit" text format, with "StackEdit" text filter.
 */
function _stackedit_save_format($overwrite = FALSE) {
  drupal_load('module', 'filter');
  /* check if exists */
  $format_exists = (bool) db_query_range('SELECT 1 FROM {filter_format} WHERE format = :format', 0, 1, array(':format' => 'stackedit'))->fetchField();
  if ($format_exists && !$overwrite) {
    return;
  }
  $format = array(
    'format' => 'stackedit',
    'name' => 'StackEdit',
    'filters' => array(
      'stackedit' => array(
        'weight' => 0,
        'status' => 1,
      ),
      'filter_html' => array(
        'weight' => 10,
        'status' => 1,
        'settings' => array(
          'allowed_html' => _stackedit_allowed_html(),
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
    ),
  );
  $format = (object) $format;
  filter_format_save($format);
}

function _stackedit_allowed_html() {
  $tags = array(
    /* link */
    'a',
    /* emphasys */
    'em',
    /* strong */
    'strong',
    /* code block */
    'pre',
    'code',
    /* block quote */
    'blockquote',
    /* unordered and ordered lists */
    'ul',
    'ol',
    'li',
    /* headings */
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'img',
    'hr'
  );
  return '<' . implode('> <', $tags) . '>';
}
