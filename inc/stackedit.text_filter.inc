<?php
/**
 * @file
 * hooks and functions relative to StackEdit text filter.
 */

/**
 * Implements hook_filter_info().
 */
function stackedit_filter_info() {
  $filters['stackedit'] = array(
    'title' => 'StackEdit',
    'description' => t('Allows content to be submitted using a <em>WYSIWYM</em> editor, with <em>Markdown</em> syntax.'),
    'process callback' => 'stackedit_filter_stackedit_process',
    'settings callback' => 'stackedit_filter_stackedit_settings',
    'tips callback'  => 'stackedit_filter_stackedit_tips',
  );
  return $filters;
}

/**
 * Filter process callback.
 *
 * Implements hook_filter_FILTER_process().
 */
function stackedit_filter_stackedit_process($text, $format) {
  $text = _stackedit_filter_process_special_url($text, $format);
  $text = _stackedit_filter_process_markdown($text, $format);
  return $text;
}

/**
 * Filter: markdown.
 */
function _stackedit_filter_process_markdown($text, $format) {
  if (!empty($text)) {
    include_once drupal_get_path('module', 'stackedit') . '/libraries/markdown/markdown.php';
    $text = Markdown($text);
  }
  return $text;
}

/**
 * Filter settings callback. Just provides a version overview.
 *
 * Implements hook_filter_FILTER_settings().
 */
function stackedit_filter_stackedit_settings($form, &$form_state, $filter, $format, $defaults) {
  /* Print Markdown library information */
  include_once drupal_get_path('module', 'stackedit') . '/libraries/markdown/markdown.php';
  $settings['markdown'] = array(
    '#type' => 'fieldset',
    '#title' => t('Markdown'),
  );
  $links = array(
    l(t('Markdown PHP Version') . ': ' . MARKDOWN_VERSION, 'http://www.michelf.com/projects/php-markdown/'),
    l(t('Markdown Extra Version') . ': ' . MARKDOWNEXTRA_VERSION, 'http://www.michelf.com/projects/php-markdown/'),
  );
  $settings['markdown']['markdown_status'] = array(
    '#title' => t('Versions'),
    '#type' => 'item',
    '#markup' => theme('item_list', array('items' => $links)),
  );
  return $settings;
}

/**
 * Filter tips.
 *
 * Implements hook_filter_FILTER_tips().
 */
function stackedit_filter_stackedit_tips($format, $long = FALSE) {
  return t('Use toolbar above text area.')
  . ' ' . t('You can use <a href="@markdown_syntax">Markdown syntax</a> to format and style the text.', array('@markdown_syntax' => 'http://daringfireball.net/projects/markdown/syntax'))
  . ' ' . l(t('Full help on StackEdit filter.'), 'admin/help/stackedit')
  . ' ' . t('Preview') . ': CTRL+P';
}

/**
 * Processes special URLs.
 */
function _stackedit_filter_process_special_url($text, $filter) {
  /* HTML comments need to be handled separately, as they may contain HTML
   * markup, especially a '>'. Therefore, remove all comment contents and add
  * them back later. */
  _filter_url_escape_comments('', TRUE);
  $text = preg_replace_callback('`<!--(.*?)-->`s', '_filter_url_escape_comments', $text);
  /* replace special urls with links to internal drupal nodes */
  $text = preg_replace_callback('`\bnid://(\d+)\b`s', '_stackedit_filter_parse_special_url_nid', $text);
  /* replace special urls with links to internal drupal users */
  $text = preg_replace_callback('`\buid://(\d+)\b`s', '_stackedit_filter_parse_special_url_uid', $text);
  /* Revert back to the original comment contents */
  _filter_url_escape_comments('', FALSE);
  $text = preg_replace_callback('`<!--(.*?)-->`', '_filter_url_escape_comments', $text);
  return $text;
}

/**
 * Used by preg_replace_callback, from nid to link.
 */
function _stackedit_filter_parse_special_url_nid($match) {
  $nid = $match[1];
  $result = db_query('SELECT n.title,n.status FROM {node} n WHERE n.nid = :nid', array(':nid' => $nid))->fetchObject();
  if ($result->status != 1) {
    return $result->title;
  }
  return l($result->title, 'node/' . $nid);
}

/**
 * Used by preg_replace_callback, from uid to link.
 */
function _stackedit_filter_parse_special_url_uid($match) {
  $uid = $match[1];
  $result = db_query('SELECT u.name FROM {users} u WHERE u.uid = :uid', array(':uid' => $uid))->fetchObject();
  return l($result->name, 'user/' . $uid);
}
