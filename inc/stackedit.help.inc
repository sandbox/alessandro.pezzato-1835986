<?php
/**
 * @file
 * provides help for StackEdit module.
 */

/**
 * Implements hook_help().
 */
function stackedit_help($path, $arg) {
  switch ($path) {
    case 'admin/help#stackedit':
      return _stackedit_help_admin_help();
  }
}

/**
 * Provides help text.
 * @return string
 *   a html string with help text.
 */
function _stackedit_help_admin_help() {
  include_once drupal_get_path('module', 'stackedit') . '/libraries/markdown/markdown.php';
  $output = '';
  $output .= '<h3>' . t('About') . '</h3>';
  $output .= '<p>' . t("StackEdit is inspired wy WMD, the editor used in StackOverflow and in all StackExchange sites.") . '<p>';
  $output .= '<p>' . t("StackEdit provides a text filter, a text format and an editor.") . '<p>';
  $output .= '<h3>Markdown</h3>';
  $output .= '<p>' . t('Markdown is a lightweight markup language, originally created by John Gruber and Aaron Swartz, allowing people to write using an easy-to-read, easy-to-write plain text format') . '</p>';
  $output .= '<ul>';
  $output .= '<li>' . l(t('Markdown PHP Version') . ': ' . MARKDOWN_VERSION, 'http://www.michelf.com/projects/php-markdown/') . '</li>';
  $output .= '<li>' . l(t('Markdown Extra Version') . ': ' . MARKDOWNEXTRA_VERSION, 'http://www.michelf.com/projects/php-markdown/') . '</li>';
  $output .= '</ul>';
  return $output;
}
