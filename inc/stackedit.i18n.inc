<?php
/**
 * @file
 * Provides intenationalization.
 */

/**
 * Provides i18n for javascript.
 */
function _stackedit_i18n_init() {
  drupal_add_js(array(
    'stackedit' => array(
      'i18n' => array(
        'Bold' => t('Bold'),
        'Italic' => t('Italic'),
        'Link' => t('Link'),
        'Quote' => t('Quote'),
        'Code block' => t('Code block'),
        'Image' => t('Image'),
        'Email' => t('Email'),
        'Ordered list' => t('Ordered list'),
        'Unordered list' => t('Unordered list'),
        'Heading 1' => t('Heading') . ' 1',
        'Heading 2' => t('Heading') . ' 2',
        'Heading 3' => t('Heading') . ' 3',
        'Horizontal Rule' => t('Horizontal Rule'),
        'bold_text_here' => t('bold_text_here'),
        'italic_text_here' => t('italic_text_here'),
        'Email address' => t('Email address'),
        'Text'
      )
    )
  ), 'setting');
}
