<?php
/**
 * @file
 * hooks and functions relative to StackEdit display of content.
 */

/**
 * Initialize stackedit display.
 */
function _stackedit_display_init() {
  $module_path = drupal_get_path('module', 'stackedit');
  /* google-code-prettify */
  $prettify_path = _stackedit_google_code_prettify_path();
  drupal_add_js($prettify_path . '/prettify.js');
  drupal_add_css($prettify_path . '/prettify.css');
  /* Elastic */
  drupal_add_js($module_path . '/libraries/jquery.elastic/jquery.elastic.source.js');
  /* code metadata */
  drupal_add_js($module_path . '/js/jquery.codeMetadata.js');
}

/**
 * Check if google-code-prettify is installed.
 * @return boolean
 *   true if installed in correct path and readable.
 */
function _stackedit_google_code_prettify_is_installed() {
  $prettify_path = realpath(_stackedit_google_code_prettify_path());
  return is_readable($prettify_path . '/prettify.js') && is_readable($prettify_path . '/prettify.css');
}

/**
 * Provides google-code-prettify installation path.
 * @return string
 *   google-code-prettify installation path.
 */
function _stackedit_google_code_prettify_path() {
  /* TODO: add support for libraries module */
  return drupal_get_path('module', 'stackedit') . '/libraries/google-code-prettify';
}
